# meetUPC - PLA D'ITERACIÓ *Inception* #

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

> En la primera iteració de la fase de inception treballarem definint els objectius principals del projecte. Per això hem de definir la planificació que es realitzarà al llarg del projecte, completant la següent informació:

>>- Definir la visió del projecte
- Determinar l'abast del projecte
- Cercar les parts interessades
- Definir el cas de negoci
- Analitzar els riscos
- Definir l'arquitectura candidata
- Definir el pla de desenvolupament de software
- Definir pla de fases i plans d'iteracions 
- Definir l'especificació de requisits

>####Dates: ####
> Les dates de les quals disposem vénen marcades per la planificació de l'assignatura, les quals són:

>>- **Data d'inici:** 23 de setembre de 2016
- **Data final:** 4 de novembre de 2016

>####Staff: ####
> Respecte a l'equip d'aquesta fase, està format per 4 persones, amb els següents rols:
>>- Analista senior: **1**
- Arquitecte: **1**
- Analista programador: **1**
- Gestor de projecte: **1** 

>I amb el següent esforç per cada un dels rols:

| ROL                 | INCEPTION | 
|---------------------|-----------| 
| Analista senior     | 65%       | 
| Arquitecte          | 10%       | 
| Analista programado | 5%        | 
| Gestor de projecte  | 20%       | 

En aquesta primera iteració, gran part de l'esforç va destinada a l'analista sènior i al gestor de projecte, encarregats de planificar el projecte i les seves fases i iteracions. D'altra banda, l'arquitecte i l'analista programador tenen un mínim esforç de cara a garantir que la planificació realitzada sigui coherent i plausible de realitzar.

## 2. COBERTURA DE CASOS D'ÚS ##

| Cas d'ús | Inception   | 
|----------|-------------| 
| CU001    | Identificat | 
| CU002    | Identificat | 
| CU003    | Identificat | 
| CU004    | Identificat | 
| CU005    | Identificat | 
| CU006    | Identificat | 
| CU007    | Identificat | 
| CU008    | Identificat | 
| CU009    | Identificat | 
| CU010    | Identificat | 
| CU011    | Identificat | 
| CU012    | Identificat | 
| CU013    | Identificat | 
| CU014    | Identificat | 
| CU015    | Identificat | 
| CU016    | Identificat | 
| CU017    | Identificat | 
| CU018    | Identificat | 
| CU019    | Identificat | 
| CU020    | Identificat | 
| CU021    | Identificat | 
| CU022    | Identificat | 
| CU023    | Identificat | 
| CU024    | Identificat | 


## 3. ACTIVITATS ##

| ID | Activitat                                     | Temps(dies) | Precedencia | 
|----|-----------------------------------------------|-------------|-------------| 
| 1  | Definir la visió del projecte                 | 5           | -           | 
| 2  | Determinar l'abast del projecte               | 5           | 1           | 
| 3  | Cercar les parts interessades                 | 20          | 2           | 
| 4  | Definir el cas de negoci                      | 15          | 2           | 
| 5  | Analitzar els riscos                          | 15          | 4           | 
| 6  | Definir l'arquitectura candidata              | 5           | 4           | 
| 7  | Definir el pla de desenvolupament de software | 12          | 5           | 
| 8  | Definir pla de fases i plans d'iteracions     | 7           | 10          | 
| 9  | Definir l'especificació de requisits          | 10          | 5           | 

 
## 4. DIAGRAMA DE GANTT ##

> ## Diagrama de Gantt: ##
> ![](diagramas/gant_1_iteracio.png)
> Aquesta taula d'excel representa el diagrama de Gantt de la primera iteració. Cada columna representa 5 dies.

> ## Diagrama de Pert: ##
> > ![](diagramas/Pert.png)
> A cada activitat se li ha assignat una lletra com a identificador (per ordre alfabètic). Els valors de les dates del diagrama estàn escalats. Una unitat representa 5 dies (una columna del diagrama de Gantt anterior).
> El camí crític que es pot observar és de 55 dies. (A>B>D>E>G) OR (A>B>D>F>I>G)