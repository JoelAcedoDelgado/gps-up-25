# meetUPC - RISCS #


## RISC 001. Plataforma/aplicació ja existent ##

### Descripció ###

Vivim en un món molt tecnològic, i no seria d’estranyar que si no hi ha cap aplicació semblant, hi hagin varies en desenvolupament.
Cal destacar que segurament tot lo que nosaltres oferirem, ja es pugui dur a terme en aplicacions separades. Per tant, la idea seria organitzar-ho tot en una i polir-ho de manera que sigui una aplicació única i altament funcional.


### Probabilitat ###

Molt probable / Probable
 
### Impacte ###

Si existeix una aplicació que ofereixi 100% de les funcionalitats que nosaltres oferim, el més probable és que una de les dues quedi obsoleta o relegada a un segon pla.
 
### Indicadors ###
> Quins indicadors objectius permeten saber que el risc ha ocorregut o està a punt d'ocórrer (secció opcional, de vegades no està clar)

En aquest cas una recerca en profunditat de les aplicacions llençades al mercat ens podria dir quines de les funcionalitats que volem oferir està ja operativa en altres sistemes.
En el cas que estigui encara en procés de desenvolupament, no podem saber-ho.

 
### Estratègies de mitigació ###
> Mesures per reduir la probabilitat d'ocurrència del risc

Trobar i implementar funcionalitats que encara no s’hagin fet, o trobar una millor manera (més eficient, més simple, fàcil, senzilla) de gestionar certes funcionalitats ja existents i desenvolupades per la competència.
 
### Plans de mitigació ###
> Mesures per reduir l'impacte del risc quan es produeix

Sempre es pot anar actualitzant i jugant amb que la aplicació és iterativa, és a dir, que podem treure/afegir funcionalitats un cop estigui llançada per l’ús públic. Amb això intentaríem sempre buscar la manera d’atraure més públic cap a la nostra banda, oferint noves funcions o millorant les actuals.










## RISC 002. Falta de participació ##

### Descripció ###
> Podria donar-se el cas que l'aplicació no aconseguís un bon nombre d'events, provocant que els estudiants deixessin d'utilitzar-la. O bé que els estudiants no participin en aquests events fent que la gent no es molestés en crear nous events.


### Probabilitat ###
>Versemblant / Poc probable

 
### Impacte ###
> Provocaria un desús de l'aplicació, ja que no compliria la funció principal d'integració entre estudiants i per tant aquests acabarien deixant d'utilitzar-la.

 
### Indicadors ###
>Si el nombre d'events creats a l'aplicació és molt baix, o bé si la participació per part dels usuaris en aquests events és molt reduïda.

 
### Estratègies de mitigació ###
> Supervisar el nombre d'events que es van creant i comprovar l'activitat mitjana en cadascun, i posant en marxa els plans de mitigació, en cas que aquestes xifres estiguin decreixent o bé siguin molt baixes.

 
### Plans de mitigació ###
> Aconseguir incentivar la creació de nous events, o bé crear events nosaltres mateixos i/o UPC per fer que la gent torni a participar-hi.




## RISC 003. Equip variable entre les diverses fases ##

### Descripció ###
> El nombre de persones que formen l'equip va variant al llarg de les diverses fases del projecte, aquest fet podria donar lloc a què el projecte s'endarrerís si la integració de les noves persones amb el grup no és l'adequada, per exemple si un programador no acaba d'entendre que ha de fer, o bé si alguna persona es posa malalta i hem de contractar un substitut.


### Probabilitat ###
>Probable

 
### Impacte ###
> Provocaria un endarreriment del projecte, i en cas que això es produeixi diverses vegades, podria suposar un greu contratemps, ja que hauríem de modificar la data d'entrega.

 
### Indicadors ###
> Si la persona no avança a la feina al ritme esperat, o bé si s'absenta prolongadament per alguna malaltia.

 
### Estratègies de mitigació ###
> Realitzar una petita formació als nous integrants de cara a què comprenguin el projecte i preguntin qualsevol dubte. A més de mantenir una bona col·laboració i per tant comunicació.
> 
### Plans de mitigació ###
> Intentar resoldre la causa de què la persona no s'estigi integrant correctament al grup de la manera més rapida possible. En cas que una persona no pugui treballar en el projecte durant un període de temps llarg, buscar un substitut el més aviar possible.
